<?php
/**
 * @package Bedmen Reseller
 * @version 1.0.0
 */
/*
Plugin Name: Bedmen Reseller
Description: Pencarian Reseller berdasarkan Propinsi, Kota ditampilkan dengan Ajax, ditautkan ke laman dengan shortcode. Shortcode [bedmen_kolom_pencarian_reseller]
Version: 1.0.0
*/

// Mencegah file ini dipanggil secara langsung
if ( !defined( 'WPINC' ) ) {
	die;
}

if ( !function_exists('boolval') ) {
    function boolval($val) {
		return (bool) $val;
    }
}



// Kolom pencarian hasil menggunakan Ajax
function kolom_pencarian_reseler_cb() {
	global $wpdb;
	$q = $wpdb->get_results( "SELECT * FROM wp_bedmen_propinsi", ARRAY_A );

	// form select daftar propinsi
	$f_propinsi = 'Propinsi <select id=\'f_propinsi\'>';
	$f_propinsi .= '<option></option>';
	foreach ($q as $v) {
		$f_propinsi .= "<option value='".$v['id']."'>".$v['nama_propinsi']."</option>".PHP_EOL;
	}
	$f_propinsi .= '</select>';

	$f_kuto = 'Kota <select id=\'f_kota\'>';
	$f_kuto .= '<option></option>';
	$f_kuto .= '</select>';
	$f_dir_url = plugin_dir_url(__FILE__);

	$f = <<<PESAN
<p>
<form>
{$f_propinsi}
</br>
{$f_kuto}
</br>
<input type="hidden" id="dir_url" value="{$f_dir_url}">
<input type="button" id="cari_reseller" value="Cari Reseller">
</form>
</br></br>
<div id="teks"></div>
</p>
PESAN;

	return $f;
	// '<div class="wpdeveloper-shortcode">Kolom pencaharian bertingkat letak di sini. Submid.</div>'.
}
add_shortcode('bedmen_kolom_pencarian_reseller','kolom_pencarian_reseler_cb');

// javascript
function wpdeveloper_enqueue_script() {
    wp_enqueue_script('wpdeveloper-script', plugins_url('bedmen_reseller.js', __FILE__), array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'wpdeveloper_enqueue_script');



/* Referensi :

*/
?>