<?php
/* --------------------------------------------------------------------------------
Ini dipanggil via Ajax
Menampilkan tabel dinamis berisi daftar reseller berdasarkan propinsi dan kota
-------------------------------------------------------------------------------- */
$path = preg_replace('/wp-content.*$/', '', __DIR__);
require_once($path.'wp-load.php');
global $wpdb;

if ($_POST['f'] == 1) {  select_kuto(); }
if ($_POST['f'] == 2) { daftar_reseller(); }

//echo 'Daftar Reseller via Ajax </br></br>';
//var_dump($_POST);

// Membuat select dropdown nama kota berdasarkan infut propinsi
function select_kuto() {
	global $wpdb;
	$id_propinsi = $_POST['id_propinsi'];
	$sql = "select * from wp_bedmen_kota where id_propinsi='".$id_propinsi."'";
	$p = $wpdb->get_results( $wpdb->prepare($sql), ARRAY_A  );

	$option = '<option></option>';
	foreach ($p as $v) {
		$option .= '<option value="'.$v['id'].'">'.$v['nama_kota'].'</option>'.PHP_EOL;
	}
	if (empty($p)) {
		$option = '<option value="">Belum ada</option>';
	}

	$select =<<<SELE
	<select id="f_kota">
		{$option}
	</select>
SELE;
	echo $select;
}

function daftar_reseller() {
	global $wpdb;

	$id_propinsi = $_POST['id_propinsi'];
	$id_kuto = $_POST['id_kuto'];

	$sql = 'select r.*, k.nama_kota, p.nama_propinsi from wp_bedmen_reseller r JOIN wp_bedmen_kota k ON r.id_kota = k.id JOIN wp_bedmen_propinsi p on k.id_propinsi = p.id';
	if ($id_propinsi) {
		$sql .= " where k.id_propinsi='".$id_propinsi."'";
	}	
	if ($id_kuto) {
		$sql .= " and r.id_kota='".$id_kuto."'";
	}

	echo $sql.'</br></br>';
	$p = $wpdb->get_results( $wpdb->prepare($sql), ARRAY_A  );
	$kolom = 1;
	echo '<table border=1><thead>Daftar Reseller</thead><tbody>'.PHP_EOL;
	foreach ($p as $v) {
		if ( $kolom == 4 ) { $kolom = 1; }
		if ( $kolom == 1 ) { echo '<tr>'; }
		echo '<td>';
		echo $v['nama_reseller'].'</br>'.$v['alamat_reseller'].'</br>';
		echo $v['nama_kota'].'</br>'.$v['nama_propinsi'].'</br>'.$v['no_wa'];
		echo '</td>';
		if ( $kolom == 3 ) { echo '</tr>'; }
		$kolom++;
	}
	echo '</tbody></table>'.PHP_EOL;
	//print_r($id_propinsi.' dan '.$id_kuto); 
}
//var_dump($p);

?>