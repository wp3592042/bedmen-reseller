jQuery(document).ready(function($) {
    // Kode JavaScript Anda di sini
    
    // select option kuto berdasarkan propinsi
    $('#f_propinsi').on('change', function() {
    	id_propinsi = $('#f_propinsi option:selected').val()
    	//alert('ID propinsi ' + id_propinsi)
    	//$('#f_kota').html("<select><option>Belum ada</option></select>")

    	$.post(
            $('#dir_url').val() + 'daftar_reseller.php',
            { dir_url: $('#dir_url').val(),
              id_propinsi: id_propinsi,
              f: '1'
            },
            function(data, status) {
                $('#f_kota').html(data)
            }
        )
    })

    // tombol cari aka submid
    $('#cari_reseller').on('click', function() {
        //$('#teks').html( $('#dir_url').val() )
        //alert($('#f_kota option:selected').val())
        $.post(
            $('#dir_url').val() + 'daftar_reseller.php',
            { dir_url: $('#dir_url').val(),
              id_propinsi: $('#f_propinsi option:selected').val(),
              id_kuto: $('#f_kota option:selected').val(),
              f: '2'
            },
            function(data, status) {
                $('#teks').html(data)
            }
        )
    })
});
