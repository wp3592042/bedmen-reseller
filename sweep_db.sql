-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Okt 2023 pada 01.35
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sweep`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `wp_bedmen_kota`
--

CREATE TABLE `wp_bedmen_kota` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `nama_kota` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `id_propinsi` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `wp_bedmen_kota`
--

INSERT INTO `wp_bedmen_kota` (`id`, `nama_kota`, `id_propinsi`) VALUES
(1, 'Kediri', 1),
(2, 'Malang', 1),
(3, 'Balaraja', 2),
(4, 'Cisauk', 2),
(5, 'Cikokol', 3),
(6, 'Mauk', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wp_bedmen_propinsi`
--

CREATE TABLE `wp_bedmen_propinsi` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nama_propinsi` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `wp_bedmen_propinsi`
--

INSERT INTO `wp_bedmen_propinsi` (`id`, `nama_propinsi`) VALUES
(1, 'Jawa Timur'),
(2, 'Banten'),
(3, 'Tangerang Selatan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wp_bedmen_reseller`
--

CREATE TABLE `wp_bedmen_reseller` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_reseller` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `alamat_reseller` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_wa` varchar(21) COLLATE latin1_general_ci NOT NULL,
  `id_kota` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `wp_bedmen_reseller`
--

INSERT INTO `wp_bedmen_reseller` (`id`, `nama_reseller`, `alamat_reseller`, `no_wa`, `id_kota`) VALUES
(1, 'Rajin Mangontank', 'Jl Utan Kayu', '6221', 1),
(2, 'Sundap Carulli', 'Jl Pademangan', '6221', 2),
(3, 'Dargom Best', 'Jl Watunas', '6221', 3),
(4, 'Isnin Khamis Ranggi', 'Jl Kallang Pudding', '6221', 2),
(5, 'CV Sorong Jaya', 'Jl Bojong Kenyod', '6221', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `wp_bedmen_kota`
--
ALTER TABLE `wp_bedmen_kota`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `wp_bedmen_propinsi`
--
ALTER TABLE `wp_bedmen_propinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `wp_bedmen_reseller`
--
ALTER TABLE `wp_bedmen_reseller`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `wp_bedmen_kota`
--
ALTER TABLE `wp_bedmen_kota`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `wp_bedmen_propinsi`
--
ALTER TABLE `wp_bedmen_propinsi`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `wp_bedmen_reseller`
--
ALTER TABLE `wp_bedmen_reseller`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
